import { Injectable } from "@angular/core";

import { of } from "rxjs";

import { categories } from "../mock/categories";

@Injectable({
    providedIn: 'root'
})
export class MockCategoryApiService {

    getCategories() {
        return of(categories);
    }
}