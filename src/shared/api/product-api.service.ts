import { Injectable } from "@angular/core";

import { of } from "rxjs";

import { TCategoryId } from "interfaces";
import { mockProducts } from "../mock/products";

@Injectable({
    providedIn: 'root'
})
export class MockProductApiService {

    getProducts(categoryId: TCategoryId) {
        return of(mockProducts.filter(product => product.category.id === categoryId));
    }
}