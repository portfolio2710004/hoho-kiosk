import { nanoid } from 'nanoid';

import { TProduct } from "interfaces";
import { categories } from './categories';

export const mockProducts: TProduct[] = [
    // 커피
    {
        id: nanoid(),
        name: '아메리카노',
        description: '따뜻한 아이스 아메리카노',
        price: 3000,
        imagePath: 'app/assets/커피_아메리카노.png',
        soldOut: false,
        category: categories.find(c => c.id === 'CO')!,
        options: []
    },
    {
        id: nanoid(),
        name: '카페라떼',
        description: '따뜻한 아이스 카페라떼',
        price: 3500,
        imagePath: 'app/assets/커피_카페라떼.png',
        soldOut: false,
        category: categories.find(c => c.id === 'CO')!,
        options: []
    },
    {
        id: nanoid(),
        name: '카페모카',
        description: '따뜻한 아이스 카페모카',
        price: 3000,
        imagePath: 'app/assets/커피_카페모카.png',
        soldOut: false,
        category: categories.find(c => c.id === 'CO')!,
        options: []
    },
    {
        id: nanoid(),
        name: '에스프레소',
        description: '따뜻한 아이스 에스프레소',
        price: 3000,
        imagePath: 'app/assets/커피_에스프레소.png',
        soldOut: false,
        category: categories.find(c => c.id === 'CO')!,
        options: []
    },
    {
        id: nanoid(),
        name: '바닐라라떼',
        description: '바닐라라라',
        price: 3000,
        imagePath: 'app/assets/커피_바닐라라떼.png',
        soldOut: false,
        category: categories.find(c => c.id === 'CO')!,
        options: []
    },
    {
        id: nanoid(),
        name: '달고나라떼',
        description: '달고나 처럼 우린 달고나',
        price: 3000,
        imagePath: 'app/assets/커피_달고나라떼.png',
        soldOut: false,
        category: categories.find(c => c.id === 'CO')!,
        options: []
    },
    // 논커피
    {
        id: nanoid(),
        name: '고구마라떼',
        description: '',
        price: 3000,
        imagePath: 'app/assets/논커피_고구마라떼.png',
        soldOut: false,
        category: categories.find(c => c.id === 'NO')!,
        options: []
    },
    {
        id: nanoid(),
        name: '곡물라떼',
        description: '',
        price: 3000,
        imagePath: 'app/assets/논커피_곡물라떼.png',
        soldOut: false,
        category: categories.find(c => c.id === 'NO')!,
        options: []
    },
    {
        id: nanoid(),
        name: '그린티라떼',
        description: '',
        price: 3000,
        imagePath: 'app/assets/논커피_그린티라떼.png',
        soldOut: false,
        category: categories.find(c => c.id === 'NO')!,
        options: []
    },
    {
        id: nanoid(),
        name: '딸기라떼',
        description: '',
        price: 3000,
        imagePath: 'app/assets/논커피_딸기라떼.png',
        soldOut: false,
        category: categories.find(c => c.id === 'NO')!,
        options: []
    },
    {
        id: nanoid(),
        name: '밀크티',
        description: '',
        price: 3000,
        imagePath: 'app/assets/논커피_밀크티.png',
        soldOut: false,
        category: categories.find(c => c.id === 'NO')!,
        options: []
    },
    {
        id: nanoid(),
        name: '블루베리라떼',
        description: '',
        price: 3000,
        imagePath: 'app/assets/논커피_블루베리라떼.png',
        soldOut: false,
        category: categories.find(c => c.id === 'NO')!,
        options: []
    },
    {
        id: nanoid(),
        name: '초코라떼',
        description: '',
        price: 3000,
        imagePath: 'app/assets/논커피_초코라떼.png',
        soldOut: false,
        category: categories.find(c => c.id === 'NO')!,
        options: []
    },
    // 주스
    {
        id: nanoid(),
        name: '복숭아주스',
        description: '',
        price: 3000,
        imagePath: 'app/assets/주스_복숭아주스.png',
        soldOut: false,
        category: categories.find(c => c.id === 'JU')!,
        options: []
    },
    {
        id: nanoid(),
        name: '샤인파인케일주스',
        description: '',
        price: 3000,
        imagePath: 'app/assets/주스_샤인파인케일주스.png',
        soldOut: false,
        category: categories.find(c => c.id === 'JU')!,
        options: []
    },
    {
        id: nanoid(),
        name: '오렌지당근주스',
        description: '',
        price: 3000,
        imagePath: 'app/assets/주스_오렌지당근주스.png',
        soldOut: false,
        category: categories.find(c => c.id === 'JU')!,
        options: []
    },
    // 스무디
    {
        id: nanoid(),
        name: '딸기요거트',
        description: '',
        price: 3000,
        imagePath: 'app/assets/스무디_딸기요거트.png',
        soldOut: false,
        category: categories.find(c => c.id === 'SM')!,
        options: []
    },
    {
        id: nanoid(),
        name: '망고요거트',
        description: '',
        price: 3000,
        imagePath: 'app/assets/스무디_망고요거트.png',
        soldOut: false,
        category: categories.find(c => c.id === 'SM')!,
        options: []
    },
    {
        id: nanoid(),
        name: '블루베리요거트',
        description: '',
        price: 3000,
        imagePath: 'app/assets/스무디_블루베리요거트.png',
        soldOut: false,
        category: categories.find(c => c.id === 'SM')!,
        options: []
    },
    {
        id: nanoid(),
        name: '플레인요거트',
        description: '',
        price: 3000,
        imagePath: 'app/assets/스무디_플레인요거트.png',
        soldOut: false,
        category: categories.find(c => c.id === 'SM')!,
        options: []
    },
    // 티
    {
        id: nanoid(),
        name: '아이스복숭아티',
        description: '',
        price: 3000,
        imagePath: 'app/assets/티_아이스복숭아티.png',
        soldOut: false,
        category: categories.find(c => c.id === 'TE')!,
        options: []
    },
    {
        id: nanoid(),
        name: '자몽티',
        description: '',
        price: 3000,
        imagePath: 'app/assets/티_자몽티.png',
        soldOut: false,
        category: categories.find(c => c.id === 'TE')!,
        options: []
    },
    {
        id: nanoid(),
        name: '자몽허니블랙티',
        description: '',
        price: 3000,
        imagePath: 'app/assets/티_자몽허니블랙티.png',
        soldOut: false,
        category: categories.find(c => c.id === 'TE')!,
        options: []
    },
    {
        id: nanoid(),
        name: '캐모마일',
        description: '',
        price: 3000,
        imagePath: 'app/assets/티_캐모마일.png',
        soldOut: false,
        category: categories.find(c => c.id === 'TE')!,
        options: []
    },
    {
        id: nanoid(),
        name: '페퍼민트',
        description: '',
        price: 3000,
        imagePath: 'app/assets/티_페퍼민트.png',
        soldOut: false,
        category: categories.find(c => c.id === 'TE')!,
        options: []
    },
    {
        id: nanoid(),
        name: '롤케이크',
        description: '',
        price: 3000,
        imagePath: 'app/assets/디저트_롤케이크.png',
        soldOut: false,
        category: categories.find(c => c.id === 'DE')!,
        options: []
    },
    {
        id: nanoid(),
        name: '마카롱',
        description: '',
        price: 3000,
        imagePath: 'app/assets/디저트_마카롱.png',
        soldOut: false,
        category: categories.find(c => c.id === 'DE')!,
        options: []
    },
    {
        id: nanoid(),
        name: '크로아상와플',
        description: '',
        price: 3000,
        imagePath: 'app/assets/디저트_크로아상와플.png',
        soldOut: false,
        category: categories.find(c => c.id === 'DE')!,
        options: []
    },
];