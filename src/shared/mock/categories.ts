import { TCategory } from "interfaces";

export const categories: TCategory[] = [
    { id: 'CO', name: '커피' },
    { id: 'NO', name: '논커피' },
    { id: 'JU', name: '주스' },
    { id: 'SM', name: '스무디' },
    { id: 'TE', name: '티' },
    { id: 'DE', name: '디저트' },
];
