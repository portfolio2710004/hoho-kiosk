import { Injectable, computed, signal } from "@angular/core";

import { TCategoryId, TCategoryState } from "interfaces";

import { EMPTY, catchError, finalize, tap } from "rxjs";

import { MockCategoryApiService } from "../api/category-api.service";

@Injectable({
    providedIn: 'root'
})
export class CategoryStore {

    private readonly state = signal<TCategoryState>({
        categories: [],
        loaded: false,
        error: null
    });

    private readonly selectedCategoryState = signal<TCategoryId>('CO');

    readonly categories = computed(() => this.state().categories);
    readonly loaded = computed(() => this.state().loaded);
    readonly error = computed(() => this.state().error);
    readonly selectedCategoryId = computed(() => this.selectedCategoryState());

    constructor(
        private readonly categoryApiService: MockCategoryApiService
    ) {
        this.getCategories().subscribe();
    }

    getCategories() {
        return this.categoryApiService.getCategories().pipe(
            tap(categories => this.state.set({
                categories,
                loaded: true,
                error: null
            })),
            catchError(error => {
                this.state.set({
                    categories: [],
                    loaded: true,
                    error: error.message
                });
                return EMPTY;
            }),
            finalize(() => console.log('getCategories() completed'))
        );
    }

    changeCategory(categoryId: TCategoryId) {
        this.selectedCategoryState.set(categoryId);
    }
}