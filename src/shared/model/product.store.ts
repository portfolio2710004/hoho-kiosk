import { Injectable, computed, signal } from "@angular/core";

import { EMPTY, catchError, finalize, tap } from "rxjs";

import { TCategoryId, TProductState } from "interfaces";

import { MockProductApiService } from "../api/product-api.service";

@Injectable({
    providedIn: 'root'
})
export class ProductStore {

    private readonly state = signal<TProductState>({
        products: [],
        loaded: false, 
        error: null
    });
    
    readonly products = computed(() => this.state().products);
    readonly loaded = computed(() => this.state().loaded);
    readonly error = computed(() => this.state().error);

    constructor(
        private readonly productApiService: MockProductApiService
    ) {
        this.getProducts().subscribe();
    }

    getProducts(categoryId: TCategoryId = 'CO') {
        return this.productApiService.getProducts(categoryId).pipe(
            tap(products => this.state.set({
                products,
                loaded: true,
                error: null
            })),
            catchError(error => {
                this.state.set({
                    products: [],
                    loaded: true,
                    error: error.message
                });
                return EMPTY;
            }),
            finalize(() => console.log('getProducts() completed'))
        );
    }

    changeCategory(categoryId: TCategoryId) {
        return this.getProducts(categoryId).subscribe();
    }
}