import { CommonModule } from "@angular/common";
import { Component, inject } from "@angular/core";

import { TCategoryId } from "interfaces";

import { CategoryStore, ProductStore } from "src/shared";

@Component({
    selector: 'app-main',
    template: `
    <div class="h-full">
        <!-- <h1 class="text-3xl">MENU</h1> -->

        <div class="flex justify-start flex-wrap">
            @for (category of categoryStore.categories(); track category.id) {
                <button  
                class="px-4 py-2 bg-white rounded-3xl m-2 text-xl"
                [ngClass]="{'text-white bg-metal': categoryStore.selectedCategoryId() === category.id}"
                (click)="changeCategory(category.id)">
                    {{category.name}}
                </button>
            }
        </div>

        <div class="grid grid-cols-3 gap-4 mt-10">
            @for (product of productStore.products(); track product.id) {
                <div class="flex flex-col items-end bg-white rounded-3xl p-4">
                    <div class="">
                        <img [src]="product.imagePath" class="w-full h-full"/>
                    </div>
                    <div class="flex flex-col justify-center items-center w-full">
                        <div>{{product.name}}</div>
                        <!-- <div>{{product.description}}</div> -->
                        <div>₩{{product.price|number}}</div>
                    </div>
                </div>
            }
        </div>

        <div class="p-10"></div>
    </div>
    `,
    standalone: true,
    imports: [
        CommonModule
    ]
})
export class MainPage {

    readonly productStore = inject(ProductStore);
    readonly categoryStore = inject(CategoryStore);

    constructor() { }

    changeCategory(categoryId: TCategoryId) {
        this.categoryStore.changeCategory(categoryId);
        this.productStore.changeCategory(categoryId);
    }
}