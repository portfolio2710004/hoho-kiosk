export enum ECategory {
    COFFEE = 'CO',
    NON_COFFEE = 'NO',
    JUICE = 'JU',
    SMOOTHY = 'SM',
    TEA = 'TE',
    DESSERT = 'DE',
}

export type TCategoryId = `${ECategory}`;

export type TCategory = {
    id: TCategoryId;
    name: string;
}