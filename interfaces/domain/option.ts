export type TOption = {
    id: string; // 옵션 ID
    name: string; // 옵션 이름 (설탕시럽, 얼음, 농도, 덜달게 ...)
    values: TOptionValue[]; // 값
}

export type TOptionValue = {
    id: string; // 옵션 값 ID
    value: string; // 값 (1펌프, 2펌프| 많이,빼고,적게 | 연하게,진하게 | 덜달게)
}