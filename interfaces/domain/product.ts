import { TCategory } from "./category";
import { TOption } from "./option";

export type TProduct = {
    id: string; // 상품 ID
    name: string; // 상품 이름
    price: number; // 상품 가격
    description: string; // 상품 설명
    imagePath: string; // 제품 이미지
    soldOut: boolean; // 재고 매진 여부
    category: TCategory; // 카테고리
    options: TOption[]; // 옵션 목록
}