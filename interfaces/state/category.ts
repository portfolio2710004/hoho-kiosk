import { TCategory } from "../domain/category";

export type TCategoryState = {
    categories: TCategory[];
    loaded: boolean;
    error: string | null;
}