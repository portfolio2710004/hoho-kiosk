import { TProduct } from "../domain/product";

export type TProductState = {
    products: TProduct[];
    loaded: boolean;
    error: string | null;
};